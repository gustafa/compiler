#ifndef NUMGLOBALS
#define NUMGLOBALS 10
#endif

#define WORKSTEALING true



#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <tgmath.h>
#include <pthread.h>

#include <time.h>

// Prototype.  The real function will be inserted by the JIT.
int16_t cell(int16_t *oldgrid, int16_t *newgrid, int16_t width, int16_t height, int16_t x, int16_t y, int16_t v, int16_t *g);


typedef struct PaddedInt32 {
  // 64 byte L1-cache block size.
  int value; // 4 bytes
  int garbage[(64 / sizeof(int)) - 1];
} PaddedInt32;

typedef struct PaddedMutex {
  pthread_mutex_t mutex;
  int16_t garbage[2 * 64 - sizeof(pthread_mutex_t)];
} PaddedMutex;


typedef struct CellThread {
  int16_t threadId;
  // int16_t numThreads;
  // int16_t iterations;
  // int16_t *oldgrid;
  // int16_t *newgrid;
  // int16_t *g;
  // int16_t gridSize;
  // pthread_barrier_t *barrier;
  // PaddedInt16 *starts;
  // PaddedInt16 *ends;
  // PaddedMutex *paddedMutexes;
} CellThread;

int16_t *oldgrid;
int16_t *newgrid;
int16_t numThreads;
int16_t gridSize;
int16_t iterations;
int16_t *g;
pthread_barrier_t barrier;
PaddedInt32 *starts;
PaddedInt32 *ends;
PaddedMutex *paddedMutexes;

int total;
int perThread;

void swapptr(int16_t** a, int16_t** b) {
    int16_t* temp = *a;
    *a = *b;
    *b = temp;
}

int stealWork(CellThread *ct, int *currentStart, int *currentEnd, int stealFrom) {
  //int16_t stealFrom = (

  int gotWork = 0;
  int decrement = 20;

    pthread_mutex_lock(&paddedMutexes[stealFrom].mutex);
      int *start = &starts[stealFrom].value;
      int *end = &ends[stealFrom].value;
      if (*start != *end) {

        *currentStart = *end - decrement;
        *currentEnd = *end;

        if (*currentStart <= *start) {
          *currentStart = *start;
        }

        *end = *currentStart;

      gotWork = 1;
      //printf("Stole work from %d!\n", stealFrom);
      }

      pthread_mutex_unlock(&paddedMutexes[stealFrom].mutex);



  return gotWork;
}


int getWork(CellThread *ct, int increment, int *currentStart, int *currentEnd, int stealFrom) {
  //printf("getting work!\n");
  int gotWork = 0;

  pthread_mutex_lock(&paddedMutexes[ct->threadId].mutex);
    int *start = &starts[ct->threadId].value;
    int *end = &ends[ct->threadId].value;


      // TODO verify, will the compiler not optimise this check away?
      if (*start != *end) {
        *currentStart = *start;
        *currentEnd = *currentStart + increment;

        if (*currentEnd > *end) {
          *currentEnd = *end;
        }

        *start = *currentEnd;
        gotWork = 1;
      }
    pthread_mutex_unlock(&paddedMutexes[ct->threadId].mutex);


  // If we're out of work ourselves attempt to steal some!

   if (!gotWork) {
    gotWork = stealWork(ct, currentStart, currentEnd, stealFrom);
    }

  return gotWork;
}




void *worker(void *args) {
  CellThread *ct = (CellThread *) args;
  // const int total = ct->gridSize * ct->gridSize;
  // const int perThread  = ceil(total / (float)ct->numThreads);

  const int myStart = ct->threadId * perThread;

  const int increments = 40;


  const int stealFrom = (ct->threadId + numThreads / 2) % numThreads;

  // Actually this is end + 1. Last thread just does whatever is remaining
  const int myEnd = ct->threadId + 1 == numThreads ? total : myStart + perThread;

  //const int16_t myXRow = myStart / ;
  //printf("Threadid  %d, global addr %p\nStart: %d, myEnd: %d, xStart: %d, yStart: %d",
  // ct->threadId, ct->g, start, myEnd, xStart, yStart);


  int done = 0;

  starts[ct->threadId].value = myStart;
  ends[ct->threadId].value = myEnd;

  // Initialise the mutex for controlling workloads.
  pthread_mutex_init(&(paddedMutexes[ct->threadId].mutex), NULL);

  int index;
  int end;
  int xRow;

  // Wait for all setup to complete.
  pthread_barrier_wait(&barrier);

  //fprintf(stderr, "%d, mystart %d, myend %d\n", ct->threadId, myStart, myEnd);


  for (int it = 0; it < iterations; ++it) {
    //printf("Iteration %d\n", it);
    // Reset looping

    #ifndef WORKSTEALING
      index = myStart;
      end = myEnd;

    #endif
    

    #ifdef WORKSTEALING
    while (getWork(ct, increments, &index, &end, stealFrom)) {
    #endif
      int xRow = index / gridSize;

      while (index < end) {
        //printf("Thread: %d,)
        //printf("executing (%d, %d)\n", xRow, index % ct->gridSize);
        newgrid[index] = cell(oldgrid, newgrid, gridSize, gridSize, xRow, index % gridSize, oldgrid[index], g);
        ++index;
        // Increment the x row when we
        if (!(index % gridSize)) {
          ++xRow;
        }
      }
    #ifdef WORKSTEALING
    }
    #endif



    pthread_barrier_wait(&barrier);
    // Reset your own start and end values ahead of the next iteration.
    #ifdef WORKSTEALING
    starts[ct->threadId].value = myStart;
    ends[ct->threadId].value = myEnd;
    #endif

    // Reset the global variables between iterations when running as one thread => possibility of global variables being in use.
    if (ct->threadId == 0) {
      for (int i = 0; i < NUMGLOBALS; ++i) {
        g[i] = 1;
      }
    }

    // Wait up for thread 0 to reset the global variables.
    pthread_barrier_wait(&barrier);


    // Reuse the old grid as the new grid
    swapptr(&newgrid, &oldgrid);
  }

  return 0;
}


void automaton(int16_t *og, int16_t *ng, int16_t size, int16_t
    itts, int16_t ts) {

  srand(time(NULL)); 


  numThreads = ts;
  iterations = itts;
  gridSize = size;
  numThreads = numThreads;
  int16_t globals[10] = {0};

  g = globals;
  oldgrid = og;
  newgrid = ng;

  //Setting up global variables and initialise them all to zero.
  CellThread cellThreads[numThreads];
  pthread_t threads[numThreads];

  starts = malloc(sizeof(PaddedInt32) * numThreads);
  ends = malloc(sizeof(PaddedInt32) * numThreads);
  paddedMutexes = malloc(sizeof(PaddedMutex) * numThreads);


  // Barrier to allow for per-iteration lock-stepping.
  pthread_barrier_init(&barrier, NULL, numThreads);

  // Malloc ends and starts.


  gridSize = size;
  total = gridSize * gridSize;
  perThread  = ceil(total / (float)numThreads);

  for (int threadId = 0; threadId < numThreads; ++threadId) {
    cellThreads[threadId].threadId = threadId;

  }

  for (int threadId =0 ; threadId < numThreads; ++threadId) {
    pthread_create(&threads[threadId], NULL, worker, &(cellThreads[threadId]));
  }

  for (int threadId = 0; threadId < numThreads; ++threadId) {
    pthread_join(threads[threadId], NULL);
  }
}
