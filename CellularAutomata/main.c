#define _XOPEN_SOURCE 600
#define DEVIATION 1
#define million 1000000

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "grammar.h"
#include "AST.h"
#include "math.h"



#ifdef __linux__
int digittoint(char c)
{
  switch (c)
  {
    case '0'...'9':
      return c - '0';
  }
}
#endif

void *CellAtomParseAlloc(void *(*mallocProc)(size_t));
void CellAtomParse(void *yyp, int yymajor, void *yyminor, void* p);
void CellAtomParseFree(void *p, void (*freeProc)(void*));

void CellAtomParseTrace(FILE *stream, char *zPrefix);

#ifdef DEBUG_LEXER
#define CellAtomParse(a,b,c,d) do {\
  fprintf(stderr, "Parsing " #b "\n");\
  CellAtomParse(a,b,c,d);\
}while(0)
#endif

static int enableTiming = 0;

static void logTimeSince(clock_t c1, char *msg)
{
  if (!enableTiming) { return; }
  clock_t c2 = clock();
  struct rusage r;
  getrusage(RUSAGE_SELF, &r);
  fprintf(stderr, "%s took %f seconds.  Peak used %ldKB.\n", msg,
    ((double)c2 - (double)c1) / (double)CLOCKS_PER_SEC, r.ru_maxrss);
}

int main(int argc, char **argv)
{
#ifdef DEBUG_PARSER
  CellAtomParseTrace(stderr, "PARSER: ");
#endif
  int runs = 1;
  int iterations = 1;
  int threads = 4;
  int useJIT = 0;
  int optimiseLevel = 0;
  int gridSize = 5;
  int maxValue = 1;
  clock_t c1;
  int c, f;
  while ((c = getopt(argc, argv, "ji:to:x:m:r:e:")) != -1) {
    switch (c) {
      case 'e':
        runs = strtol(optarg, 0, 10);
        break;
      case 'j':
        useJIT = 1;
        break;
      case 'x':
        gridSize = strtol(optarg, 0, 10);
        break;
      case 'r':
        threads = strtol(optarg, 0, 10);
        break;
      case 'm':
        maxValue = strtol(optarg, 0, 10);
        break;
      case 'i':
        iterations = strtol(optarg, 0, 10);
        break;
      case 't':
        enableTiming = 1;
        break;
      case 'o':
        optimiseLevel = strtol(optarg, 0, 10);
    }
  }

  void *parser = CellAtomParseAlloc(malloc);
  char ch;
  struct statements *result;
  while ((ch = getchar()) != EOF) {
resume_parsing:
    if (isspace(ch)) continue;
    switch (ch)
    {
      case '+': CellAtomParse(parser, PLUS, 0, &result); break;
      case '-': CellAtomParse(parser, SUB, 0, &result); break;
      case '*': CellAtomParse(parser, MUL, 0, &result); break;
      case '/': CellAtomParse(parser, DIV, 0, &result); break;
      case '=': CellAtomParse(parser, EQ, 0, &result); break;
      case '>': CellAtomParse(parser, GT, 0, &result); break;
      case '(': CellAtomParse(parser, LBR, 0, &result); break;
      case ')': CellAtomParse(parser, RBR, 0, &result); break;
      case '[': CellAtomParse(parser, LSQ, 0, &result); break;
      case ']': CellAtomParse(parser, RSQ, 0, &result); break;
      case ',': CellAtomParse(parser, COMMA, 0, &result); break;
      case '|': CellAtomParse(parser, BAR, 0, &result); break;
      case 'n': {
        if (!(('e' == getchar()) &&
              ('i' == getchar()) &&
              ('g' == getchar()) &&
              ('h' == getchar()) &&
              ('b' == getchar()) &&
              ('o' == getchar()) &&
              ('u' == getchar()) &&
              ('r' == getchar()) &&
              ('s' == getchar()))) {
          fprintf(stderr, "Unhelpful parser error!\n");
          exit(-1);
        }
        CellAtomParse(parser, NEIGHBOURS, 0, &result);
        break;
      }
      case 'm': {
        ch = getchar();
        if (ch == 'i' && getchar() == 'n') {
          CellAtomParse(parser, MIN, 0, &result);
        } else if (ch == 'a' && getchar() == 'x') {
          CellAtomParse(parser, MAX, 0, &result);
        } else {
          fprintf(stderr, "Unhelpful parser error!\n");
          exit(-1);
        }
        break;
      }
      case 'a': {
        ch = getchar();
        if (!isdigit(ch)) {
          fprintf(stderr, "Unhelpful parser error!\n");
          exit(-1);
        }
        uintptr_t regnum = (digittoint(ch) << 2) | 3;
        CellAtomParse(parser, REGISTER, (void*)regnum, &result);
        break;
      }
      case 'g': {
        ch = getchar();
        if (!isdigit(ch)) {
          fprintf(stderr, "Unhelpful parser error!\n");
          exit(-1);
        }
        uintptr_t regnum = ((digittoint(ch) + 10) << 2) | 3;
        CellAtomParse(parser, REGISTER, (void*)regnum, &result);
        break;
      }
      case 'v': {
        uintptr_t regnum = (21 << 2) | 3;
        CellAtomParse(parser, REGISTER, (void*)regnum, &result);
        break;
      }
      default: {
        if (isdigit(ch)) {
          intptr_t literal = 0;
          do {
            literal *= 10;
            literal += digittoint(ch);
            ch = getchar();
          } while (isdigit(ch));
          literal <<= 2;
          literal |= 1;
          CellAtomParse(parser, NUMBER, (void*)literal, &result);
          // We've consumed one more character than we should, so skip back to
          // the top of the loop without getting a new one.
          goto resume_parsing;
        }
        fprintf(stderr, "Unhelpful parser error!\n");
        exit(-1);
      }
    }
  }
  CellAtomParse(parser, 0, 0, &result);
  CellAtomParseFree(parser, free);
#ifdef DUMP_AST
  for (uintptr_t i=0 ; i<result->count ; i++) {
    printAST(result->list[i]);
    putchar('\n');
  }
#endif
  /*
  int16_t oldgrid[] = {
     0,0,0,0,0,
     0,0,0,0,0,
     0,1,1,1,0,
     0,0,0,0,0,
     0,0,0,0,0
  };
  int16_t newgrid[25];
  */
  int16_t *g1 = malloc(gridSize * sizeof(int16_t) * gridSize);
  for (int i=0 ; i<(gridSize*gridSize) ; i++) {
    g1[i] = random() % (maxValue + 1);
  }
  int16_t *g2 = malloc(gridSize * sizeof(int16_t) * gridSize);
  c1 = clock();
  logTimeSince(c1, "Generating random grid");
  int i=0;
  if (useJIT) {
    struct timeval before;
    struct timeval after;
    printf("JIT!\n");
    c1 = clock();
    // int16_t usesGlobal = 0;
    automaton ca = compile(result->list, result->count, optimiseLevel);
    logTimeSince(c1, "Compiling");
    c1 = clock();

    //printf("\n g1 orig: %p; g2 orig: %p \n", g1, g2);

    // Enforce only one thread be used when the program contains a global variable.
    // if (usesGlobal) {
    //   threads = 1;
    // }

    long total = 0;

    printf("number of threads: %d\n", threads);

    if (DEVIATION) {
      printf("deviation\n");
      unsigned long runtimes[runs];
      unsigned long total = 0;
      gettimeofday(&before, NULL);
      for (int i = 0; i < runs; ++i) {
        gettimeofday(&before, NULL);
        ca(g1, g2, gridSize, iterations, threads);
        gettimeofday(&after, NULL);
        unsigned long ns = (after.tv_sec - before.tv_sec) * million + (after.tv_usec - before.tv_usec);
        runtimes[i] = ns;
        total += ns;
      }

      float average = total / (float) runs;

      float varianceSum = 0;

      for (int i = 0; i < runs; ++i) {
        float deviation = average - runtimes[i];
        varianceSum += (deviation * deviation);
      }


      float variance = (varianceSum / iterations) / (float) million;
      //fprintf(stderr, "Total! %lu\n", total);

      fprintf(stderr, "{\n");
      fprintf(stderr, "\"iterations\": %d,\n", iterations);
      fprintf(stderr, "\"threads\": %d,\n", threads);
      fprintf(stderr, "\"size\": %d,\n", gridSize);

      fprintf(stderr, "\"runs\": %d,\n", runs);

      // fprintf(stderr, "\"variance\": %f,\n", variance);
      // fprintf(stderr, "\"std\": %f,\n", sqrt(variance));
            //fprintf(stderr, "\"std\": %f,\n", sqrt(variance) / (float) million);



      fprintf(stderr, "\"run_times\": [");
      for (int i = 0; i < runs; ++i) {
        fprintf(stderr, "%f", runtimes[i] / (float) million);
        if (i != runs - 1) {
          fprintf(stderr, ", ");
        } else {
          fprintf(stderr, "],\n");
        }
      }
      fprintf(stderr, "}\n");


    } else {
      printf("NO DEVIATION!\n");
      gettimeofday(&before, NULL);
      for (int i = 0; i < runs; ++i) {
        ca(g1, g2, gridSize, iterations, threads);
      }
      gettimeofday(&after, NULL);
    }

    long s = after.tv_sec - before.tv_sec;
    long ms = after.tv_usec - before.tv_usec;

    if (ms < 0) {
      ms = 1000000 + ms;
      s--;
    }

    double timing = s + (ms / (double)1000000);
    double perRun = timing / runs;







    //fprintf(stderr, "Runs: %d\nExecution time: %f\nPer run: %f\n", runs, s,ms, timing, perRun);


    logTimeSince(c1, "Running compiled version");
  } else {
    c1 = clock();
    for (int i=0 ; i<iterations ; i++) {
      int16_t *tmp = g1;
      runOneStep(g1, g2, gridSize, gridSize, result->list, result->count);
      // g1 = g2;
      // g2 = tmp;
    }
    logTimeSince(c1, "Interpreting");
  }

  // For odd iterations result is in g1, even in g2.

  int16_t *resultGrid = iterations % 2 ? g2 : g1;
  if (1) {
    for (int x=0 ; x<gridSize ; x++) {
      for (int y=0 ; y<gridSize ; y++) {
        printf("%d ", resultGrid[i++]);
      }
      putchar('\n');
    }
  }
  return 0;
}
